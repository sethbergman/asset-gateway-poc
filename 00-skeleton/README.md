# 00-skeleton stage

This directory is the non-OAX specific starting point for our development.
It shows what files come from our choice of development tools and framework by default.

Having quick access to this state allows us to quickly run experiments in a clean environment.
We can test out tool and framework upgrades or prepare minimal examples for issue reproduction,
when preparing bug reports.
