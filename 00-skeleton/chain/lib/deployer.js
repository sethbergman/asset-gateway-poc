const {create} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        MetaCoin,
        Lab
    } = contractRegistry

    const metaCoin = {} // await deploy(MetaCoin)
    const lab = await deploy(Lab)
    return {metaCoin, lab}
}

module.exports = {
    base
}
