pragma solidity ^0.4.19;


import "oax.sol";


contract Memberships {
    OpenANXToken oaxToken;

    function Memberships(OpenANXToken _oaxToken) public {
        oaxToken = _oaxToken;
    }

    mapping (uint8 => uint8) public membershipPrice;

    mapping (uint8 => mapping (address => bool)) public members;

    mapping (address => bool) public votingMembers;

    event Temp(uint figure, uint price);

    function addMember(uint8 membershipType) public returns (bool){
        return addMemberFor(membershipType, msg.sender);
    }

    function removeMember(uint8 membershipType) public returns (bool){
        return removeMemberFor(membershipType, msg.sender);
    }

    function transferMember(uint8 membershipType, address to) public {
        return transferMemberFor(membershipType, msg.sender, to);
    }

    //TODO add auth modifier
    function addMembershipType(uint8 membershipType, uint8 price) public {
        require(membershipPrice[membershipType] == 0);
        membershipPrice[membershipType] = price;
    }

    //TODO add auth modifier
    function addMemberFor(uint8 membershipType, address member) public returns (bool){
        //oax contract . burn(member, membershipPrice[membershipType])
        require(membershipPrice[membershipType] > 0);
        Temp(oaxToken.allowance(member, address(this)), membershipPrice[membershipType]);
        require(oaxToken.allowance(member, address(this)) >= membershipPrice[membershipType]);
        members[membershipType][member] = true;
        return members[membershipType][member];
    }

    //TODO add auth modifier
    function removeMemberFor(uint8 membershipType, address member) public returns (bool){
        require(membershipPrice[membershipType] > 0);
        members[membershipType][member] = false;
        return members[membershipType][member];
    }

    //TODO add auth modifier
    function transferMemberFor(uint8 membershipType, address from, address to) public {
        require((membershipPrice[membershipType] > 0) && (members[membershipType][from]));
        members[membershipType][from] = false;
        members[membershipType][to] = true;
    }

    function oaxAllowance(address _owner, address _spender) public constant returns (uint){
        return oaxToken.allowance(_owner, _spender);
    }

}