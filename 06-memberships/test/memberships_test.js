const {
    expect,
    expectAsyncThrow,
    expectNoAsyncThrow,
    expectThrow,
    toBN,
    solc,
    ganacheWeb3
} = require('chain-dsl/test/helpers')

const {
    address,
    wad,
    send,
    call,
    txEvents
} = require('chain-dsl')

const solcInput = require('../solc-input.json')
const deployer = require('../lib/deployer')

const addMembershipType = 'addMembershipType(uint8,uint8)'
const addParticipantMember = 'addMemberFor(uint8,address)'
const addVotingMember = 'addMemberFor(uint8,address)'
const transferParticipantMember = 'transferMemberFor(uint8,address,address)'
const removeParticipantMember = 'removeMemberFor(uint8,address)'
const removeVotingMember = 'removeMemberFor(uint8,address)'
const participantMembers = 'members(uint8,address)'
const votingMembers = 'members(uint8,address)'
const mint = 'mint(address,uint256)'
const withdraw = 'withdraw'
const burn = 'burn(address,uint256)'
const deposit = 'deposit'
const approve = 'approve'
const transfer = 'transfer'
const transferFrom = 'transferFrom'
const lastLimitResetTime = 'lastLimitResetTime'
const PARTICIPANT_MEMBERSHIP_NAME = 1
const VOTING_MEMBERSHIP_NAME = 2
const NEW_MEMBERSHIP_NAME = 3

function hours(hrs) {
    return hrs * 1000 * 60 * 60
}

describe.only("Memberships:", function () {
    let web3, snaps, accounts, memberships, oaxToken,
        DEPLOYER,
        OPERATOR,
        CUSTOMER,
        CUSTOMER_TWO,
        MIN_AMT,
        AMT,
        DEFAULT_DAILY_LIMIT

    before('deployment', async () => {
        this.timeout(6000)
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER,
            CUSTOMER_TWO
        ] = accounts = await web3.eth.getAccounts()

        MIN_AMT = wad(1)
        AMT = wad(100)
        DEFAULT_DAILY_LIMIT = wad(10000)

        ;({memberships, oaxToken} = await deployer.base(web3, solc(__dirname, '../solc-input.json'),
            DEPLOYER,
            OPERATOR,
            DEFAULT_DAILY_LIMIT))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    context("extensibility", function () {

        it("can add multiple memberships", async () => {
            await expectNoAsyncThrow(async () => {
                await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
                await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, AMT * 2)
            })
        })

        it("existing memberships should not be overwritten", async () => {
            await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
            await expectAsyncThrow(async () => {


                await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
            })
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT * 2)
            })
        })

        it("different memberships can have same scale", async () => {
            await expectNoAsyncThrow(async () => {
                await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
                await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, AMT)
            })
        })
    })

    context("empowerment", function () {
        beforeEach(async () => {
            await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
            await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, AMT)
            await send(oaxToken, DEPLOYER, "kycVerify", OPERATOR)
            await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER)
            //FIXME
            // await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, AMT*3)
        })

        it("empower customer as Participant Member", async () => {
            await send(oaxToken, CUSTOMER, "approve", address(memberships), AMT)

            let events = await (txEvents(await send(memberships, OPERATOR, addParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)))
            console.dir(events)
            let memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            expect(memberStatus).eql(true)
        })

        it("DO NOT empower customer as Participant Member when allowance not enough", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, addParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                let memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                expect(memberStatus).eql(true)
            })

        })

        it("DO NOT empower customer as Participant Member when allowance enough but burn fails", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, addParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                let memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                expect(memberStatus).eql(true)
            })
        })

        it("remove customer as Participant Member", async () => {
            await send(oaxToken, CUSTOMER, "approve", address(memberships), AMT)
            await send(memberships, OPERATOR, addParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            let memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            expect(memberStatus).eql(true)
            await send(memberships, OPERATOR, removeParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            expect(memberStatus).eql(false)
        })

        it("empower customer as Voting Member", async () => {
            await send(oaxToken, CUSTOMER, "approve", address(memberships), AMT * 2)
            await send(memberships, OPERATOR, addVotingMember, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            let memberStatus = await call(memberships, votingMembers, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            expect(memberStatus).eql(true)
        })

        it("remove customer as Voting Member", async () => {
            await send(oaxToken, CUSTOMER, "approve", address(memberships), AMT * 2)
            await send(memberships, OPERATOR, addVotingMember, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            let memberStatus = await call(memberships, votingMembers, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            expect(memberStatus).eql(true)
            await send(memberships, OPERATOR, removeVotingMember, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            memberStatus = await call(memberships, votingMembers, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            expect(memberStatus).eql(false)
        })

        it("can't empower customer with non-existing membership", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, addVotingMember, NEW_MEMBERSHIP_NAME, CUSTOMER)
            })
        })
    })

    context("transferability", function () {
        beforeEach(async () => {
            await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
            await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, AMT)
        })

        it("transfers Participant Membership from customer to customer_two", async () => {
            await send(oaxToken, CUSTOMER, "approve", address(memberships), AMT)
            await send(memberships, OPERATOR, addParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            await send(memberships, OPERATOR, transferParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER, CUSTOMER_TWO)
            let memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            expect(memberStatus).eql(true)
        })

        it("CANNOT transfers Participant Membership from customer to customer_two if customer has no such", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, transferParticipantMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER, CUSTOMER_TWO)
            })
            let memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            expect(memberStatus).eql(false)
            memberStatus = await call(memberships, participantMembers, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            expect(memberStatus).eql(false)
        })
    })

    context("OAX payment", function () {
        it("takes OAX and burns it", async () => {
            let oaxContractAddress, oaxContractAbi
        })
    })

    context("Initial config", function () {
        it("can call OAX token sale contract", async () => {
            let oaxAllowance = await call(memberships, "oaxAllowance", CUSTOMER, CUSTOMER_TWO)
            expect(oaxAllowance).eq(0)
        })
        it("can get legit allowance figures", async () => {
            await send(oaxToken, OPERATOR, "approve", CUSTOMER, AMT)
            let oaxAllowance = await call(memberships, "oaxAllowance", OPERATOR, CUSTOMER)
            expect(oaxAllowance).eq(AMT)
            oaxAllowance = await call(memberships, "oaxAllowance", CUSTOMER, OPERATOR)
            expect(oaxAllowance).eq(0)
        })
    })
})
