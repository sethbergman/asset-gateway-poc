const {
    address,
    wad,
    sig,
    send,
    call,
    create
} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER, OPERATOR, LIMIT = wad(10000)) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        OpenANXToken,
        Memberships
    } = contractRegistry

    const oaxToken = await deploy(OpenANXToken, OPERATOR)
    const memberships = await deploy(Memberships, oaxToken.options.address)

    return {
        oaxToken,
        memberships
    }
}

module.exports = {
    base
}
