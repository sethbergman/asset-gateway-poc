const {
    expect,
    expectThrow,
    toBN,
    solc,
    ganacheWeb3,
    ZERO_ADDR,
} = require('chain-dsl/test/helpers')
const {distillEvent, txEvents, balance, send} = require('chain-dsl')
const deploy = require('../lib/deployer')

const mintFor = 'mintFor'
const mint = 'mint(address,uint256)'
const mintForSelf = 'mint(uint256)'
const burnFrom = 'burnFrom'
const deposit = 'deposit'
const approve = 'approve'
const transfer = 'transfer'
const transferFrom = 'transferFrom'

describe('Gate', () => {
    let web3, snaps, accounts, gate,
        DEPLOYER,
        OPERATOR,
        CUSTOMER,
        CUSTOMER1,
        CUSTOMER2

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER,
            CUSTOMER1,
            CUSTOMER2
        ] = accounts = await web3.eth.getAccounts()

        ;({gate} = await deploy.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER, OPERATOR))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    context('DEPLOYER', () => {
        it('cannot mint for customer', async () => {
            await expectThrow(async () => {
                await send(gate, DEPLOYER, mintFor, CUSTOMER, 123)
            })
        })
    })

    context('CUSTOMER', () => {
        const pendingDeposits = async (customer) =>
            gate.getPastEvents('DepositRequested',
                {fromBlock: 0, filter: {by: customer}})
                .map(distillEvent)

        it('cannot mint for themselves', async () => {
            await expectThrow(async () => {
                await send(gate, CUSTOMER, mint, CUSTOMER, 123)
            })
        })

        it('cannot mint for others', async () => {
            await expectThrow(async () => {
                await send(gate, CUSTOMER1, mint, CUSTOMER2, 123)
            })
        })

        it('cannot mint for an operator', async () => {
            await expectThrow(async () => {
                await send(gate, CUSTOMER, mint, OPERATOR, 123)
            })
        })

        it('cannot burn for themselves', async () => {
            await expectThrow(async () => {
                await send(gate, CUSTOMER, burnFrom, CUSTOMER, 123)
            })
        })

        it("can request a deposit", async () => {
            const AMT = toBN(10)

            const events = await txEvents(send(gate, CUSTOMER, deposit, AMT))

            expect(events).containSubset([{
                NAME: 'DepositRequested',
                by: CUSTOMER,
                amount: AMT.toString(10)
            }])
        })

        it("can list her deposits, but not others'", async () => {
            // FIXME On the chain anyone can see deposits
            const AMT_C1_1 = toBN(10)
            const AMT_C1_2 = toBN(20)
            const AMT_C2_1 = toBN(30)
            await send(gate, CUSTOMER1, deposit, AMT_C1_1)
            await send(gate, CUSTOMER1, deposit, AMT_C1_2)
            await send(gate, CUSTOMER2, deposit, AMT_C2_1)

            const deposits = await pendingDeposits(CUSTOMER1)

            expect(deposits).containSubset([{
                NAME: 'DepositRequested',
                by: CUSTOMER1,
                amount: AMT_C1_1.toString(10)
            }, {
                NAME: 'DepositRequested',
                by: CUSTOMER1,
                amount: AMT_C1_2.toString(10)
            }])

            expect(deposits).not.containSubset([{
                NAME: 'DepositRequested',
                by: CUSTOMER2,
                amount: AMT_C2_1.toString(10)
            }])
        })

        it("can claim control of deposit if approved", async () => {
            await send(gate, OPERATOR, mintForSelf, 10)
            await send(gate, OPERATOR, 'approve', CUSTOMER, 3)
            await send(gate, CUSTOMER, transferFrom, OPERATOR, CUSTOMER, 1)
            await send(gate, CUSTOMER, transferFrom, OPERATOR, CUSTOMER, 2)

            expect(await balance(gate, CUSTOMER)).eq(3)
            expect(await balance(gate, OPERATOR)).eq(7)
        })

        it("can not claim control of deposit without approval", async () => {
            await send(gate, OPERATOR, mintForSelf, 10)

            await expectThrow(async () =>
                await send(gate, CUSTOMER, transferFrom, OPERATOR, CUSTOMER, 1))
        })
    })

    context('OPERATOR', () => {
        const pendingDeposits = async (customer) =>
            gate.getPastEvents('DepositRequested', {fromBlock: 0})
                .map(distillEvent)

        const pendingWithdrawals = async () => {
            const withdrawalAndMintTransfers =
                await gate.getPastEvents('Transfer',
                    {
                        fromBlock: 0,
                        filter: {dst: OPERATOR}
                    })
                    .map(distillEvent)
            const isTransferByMint = (ev) => ev.src !== ZERO_ADDR
            const withdrawalTransfers = withdrawalAndMintTransfers.filter(isTransferByMint)

            const burns =
                await gate.getPastEvents('Burn', {fromBlock: 0})
                    .map(distillEvent)

            const withdrawals =
                await gate.getPastEvents('Withdrawn', {fromBlock: 0})
                    .map(distillEvent)

            return withdrawalTransfers.concat(burns).concat(withdrawals)
        }

        it('can mint', async () => {
            await send(gate, OPERATOR, mintFor, CUSTOMER, 123)
        })

        it('is the only actor who can mint', async () => {
            await expectThrow(async () => {
                await send(gate, CUSTOMER, mintFor, CUSTOMER, 123)
            })
        })

        it("can list pending deposits", async () => {
            const AMT1 = toBN(10)
            const AMT2 = toBN(20)

            await send(gate, CUSTOMER1, deposit, AMT1)
            await send(gate, CUSTOMER2, deposit, AMT2)

            const deposits = await pendingDeposits()

            expect(deposits).containSubset([
                {NAME: 'DepositRequested', by: CUSTOMER1, amount: AMT1.toString(10)},
                {NAME: 'DepositRequested', by: CUSTOMER2, amount: AMT2.toString(10)}
            ])
        })

        it("can mint tokens when received fiat", async () => {
            const AMT = toBN(10)
            await send(gate, CUSTOMER1, deposit, AMT)

            const events = await txEvents(send(gate, OPERATOR, mintFor, CUSTOMER, AMT))

            expect(events).containSubset([
                {NAME: 'Mint', guy: OPERATOR, wad: AMT.toString(10)},
                {NAME: 'Transfer', src: ZERO_ADDR, dst: OPERATOR, wad: AMT.toString(10)},
                {NAME: 'Transfer', src: OPERATOR, dst: CUSTOMER, wad: AMT.toString(10)}
            ])
        })

        it("can list pending withdrawals", async () => {
            const AMT1 = toBN(10)
            const AMT2 = toBN(20)

            await send(gate, CUSTOMER1, deposit, AMT1)
            await send(gate, CUSTOMER2, deposit, AMT2)

            await send(gate, OPERATOR, mintFor, CUSTOMER1, AMT1)
            await send(gate, OPERATOR, mintFor, CUSTOMER2, AMT2)

            await send(gate, CUSTOMER1, transfer, OPERATOR, AMT1)
            await send(gate, CUSTOMER2, transfer, OPERATOR, AMT2)

            const withdrawals = await pendingWithdrawals()

            expect(withdrawals.length).equal(2)
            expect(withdrawals).containSubset([
                {NAME: 'Transfer', src: CUSTOMER1, dst: OPERATOR, wad: AMT1.toString(10)},
                {NAME: 'Transfer', src: CUSTOMER2, dst: OPERATOR, wad: AMT2.toString(10)}
            ])
        })

        it("can burn tokens", async () => {
            const AMT = toBN(10)
            await send(gate, CUSTOMER, deposit, AMT)
            await send(gate, OPERATOR, mintFor, CUSTOMER, AMT)
            await send(gate, CUSTOMER, transfer, OPERATOR, AMT)
            await send(gate, OPERATOR, burnFrom, CUSTOMER, AMT)

            const withdrawals = await pendingWithdrawals()

            expect(withdrawals).containSubset([
                {NAME: 'Burn', guy: OPERATOR, wad: AMT.toString(10)},
                {NAME: 'Withdrawn', from: CUSTOMER, amount: AMT.toString(10)}
            ])
        })
    })
})
