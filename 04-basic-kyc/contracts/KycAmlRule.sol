pragma solidity ^0.4.8;


contract KycAmlRule {
    // Checks whether the appropriate KYC has been done for the sender and receiver.
    function canDeposit(address _asset, address _to, uint _value)
    public
    view
    returns (bool);
    // Checks whether the appropriate KYC has been done for the sender and receiver.
    function canTransfer(address _asset, address _from, address _to, uint _value)
    public
    view
    returns (bool);
    // Checks whether the appropriate KYC has been done for the sender and receiver.
    function canWithdraw(address _asset, address _from, uint _value)
    public
    view
    returns (bool);
}
