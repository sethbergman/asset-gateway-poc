pragma solidity ^0.4.16;


import "ds/auth.sol";
import "ds/token.sol";
import "AccessControl.sol";
import "KycAmlToken.sol";


contract Gate is DSToken, KycAmlToken {
    event DepositRequested(address indexed by, uint256 amount);
    event WithdrawalRequested(address indexed from, uint256 amount);
    event Withdrawn(address indexed from, uint256 amount);

    function Gate(AccessControl ac, KycAmlRule kycAmlRule)
    DSToken("TOKUSD")
    KycAmlToken(kycAmlRule)
    public
    {
        //        uint8 role = ac.OPERATOR();
        //        bytes4 method = bytes4(keccak256("mintFor(address,uint256)"));
        //        ac.setRoleCapability(role, this, method, true);
        setAuthority(ac);
        setOwner(0x0);
    }

    function transferFrom(address src, address dst, uint wad)
    checkTransfer(src, dst, wad)
    public
    returns (bool)
    {
        return super.transferFrom(src, dst, wad);
    }

    function deposit(uint256 amount) checkDeposit(amount) public {
        DepositRequested(msg.sender, amount);
    }

    function mintFor(address to, uint256 amount) checkDepositTo(to, amount) public {
        mint(msg.sender, amount);
        /* Because the EIP20 standard says so, we emit a Transfer event:
           A token contract which creates new tokens SHOULD trigger a
           Transfer event with the _from address set to 0x0 when tokens are created.
            (https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md)
        */
        Transfer(0x0, msg.sender, amount);
        transfer(to, amount);
    }

    function withdraw(uint256 amount) checkWithdraw(amount) public {
        WithdrawalRequested(msg.sender, amount);
    }

    function burnFrom(address from, uint256 amount) checkWithdrawFrom(from, amount) public {
        burn(amount);
        Withdrawn(from, amount);
    }

    function setKycVerified(address user, bool isKycVerified) public auth {
        kycVerified[user] = isKycVerified;
    }
}
