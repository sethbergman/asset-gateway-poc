#!/usr/bin/env bash

for pkg in \
    00-skeleton/chain \
    01-fiat-token/chain \
    02-money-in/chain \
    03-limits \
    03-money-out \
    04-basic-kyc \
    05-upgradability/0-goal \
    05-upgradability/1-base \
    05-upgradability/2-upgraded \
    06-memberships \
    99-latest \
    chain-dsl
do
    (cd $pkg; pwd; pnpm test)
done
