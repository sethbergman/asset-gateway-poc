import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LocalizeRouterModule, LocalizeRouterSettings, LocalizeParser } from 'localize-router';
import { LocalizeRouterHttpLoader } from 'localize-router-http-loader';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {GatewayDetailComponent} from './gateway/gateway-detail/gateway-detail.component';
import {GatewayDashboardComponent} from './gateway/gateway-dashboard/gateway-dashboard.component';
import { GatewaySubmitComponent} from './gateway/gateway-submit/gateway-submit.component';
import { GatewayTransferComponent } from './gateway/gateway-transfer/gateway-transfer.component';
import { GatewayMintComponent } from './gateway/gateway-mint/gateway-mint.component';
import {LoginComponent} from './user/login/login.component';
import {RegisterComponent} from './user/register/register.component';
import {RegisterDoneComponent} from "./user/register/register-done.component";



import {OverviewComponent} from './admin/overview/overview.component';
import {VerifyRequestComponent} from './admin/verify-request/verify-request.component';
import {MintTokenComponent} from './admin/mint-token/mint-token.component';
import {TansferRequestComponent} from "./admin/tansfer-request/tansfer-request.component";
import {BurnRequestComponent} from "./admin/burn-request/burn-request.component";
import {WithdrawComponent} from "./admin/withdraw/withdraw.component";
import {KycprocessRequestComponent} from "./admin/kycprocess-request/kycprocess-request.component";
import {KycprocessComponent} from "./admin/kycprocess/kycprocess.component";
import {GatewayWithdrawMintComponent} from "./gateway/gateway-withdraw-mint/gateway-withdraw-mint.component";
import {GatewayWithdrawTransferComponent} from "./gateway/gateway-withdraw-transfer/gateway-withdraw-transfer.component";
import {GatewayWithdrawSubmitComponent} from "./gateway/gateway-withdraw-submit/gateway-withdraw-submit.component";
import {GatewayKycprocessComponent} from "./gateway/gateway-kycprocess/gateway-kycprocess.component";
import {GatewayKycprocessVerifyComponent} from "./gateway/gateway-kycprocess-verify/gateway-kycprocess-verify.component";

export function HttpLoaderFactory(translate: TranslateService, location: Location, settings: LocalizeRouterSettings, http: HttpClient) {
  return new LocalizeRouterHttpLoader(translate, location, settings, http, 'i18n/locales.json');
}

const routes = [
  { path: '', component: HomeComponent },
  { path: 'gateway/detail', component: GatewayDetailComponent },
  {
    path: 'gateway/dashboard',
    component: GatewayDashboardComponent
  }, {
    path: 'gateway/kycprocess',
    component: GatewayKycprocessComponent
  }, {
    path: 'gateway/kycprocess/verify',
    component: GatewayKycprocessVerifyComponent
  }, {
    path: 'gateway/dashboard/deposit/submit',
    component: GatewaySubmitComponent,
  }, {
    path: 'gateway/dashboard/deposit/transfer',
    component: GatewayTransferComponent,
  }, {
    path: 'gateway/dashboard/deposit/mint',
    component: GatewayMintComponent,
  }, {
    path: 'gateway/dashboard/withdraw/submit',
    component: GatewayWithdrawSubmitComponent
  }, {
    path: 'gateway/dashboard/withdraw/transfer',
    component: GatewayWithdrawTransferComponent
  }, {
    path: 'gateway/dashboard/withdraw/mint',
    component: GatewayWithdrawMintComponent
  }, {
    path: 'admin/overview',
    component: OverviewComponent,
  }, {
    path: 'admin/kycprocess',
    component: KycprocessComponent
  }, {
    path: 'admin/kycprocess-request',
    component: KycprocessRequestComponent
  }, {
    path: 'admin/verify-request/:id',
    component: VerifyRequestComponent,
  }, {
    path: 'admin/mint-token/:id',
    component: MintTokenComponent,
  }, {
    path: 'admin/withdraw',
    component: WithdrawComponent,
  }, {
    path: 'admin/burn-request/:id',
    component: BurnRequestComponent
  }, {
    path: 'admin/tansfer-request',
    component: TansferRequestComponent
  },
  {
    path: 'account/register', component: RegisterComponent
  },
  { path: 'account/register-done', component: RegisterDoneComponent },
  { path: 'account/login', component: LoginComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: HttpLoaderFactory,
        deps: [TranslateService, Location, LocalizeRouterSettings, HttpClient]
      }
    })
  ],
  exports: [ RouterModule, LocalizeRouterModule ]
})
export class AppRoutingModule {}
