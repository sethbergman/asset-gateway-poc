import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Web3Service} from "../../util/web3.service";
import {OAuthService} from 'angular-oauth2-oidc';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {DepositRequest, DepositService} from "../../services/deposit.service";
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-gateway-dashboard',
  templateUrl: './gateway-dashboard.component.html',
  styleUrls: ['./gateway-dashboard.component.scss']
})
export class GatewayDashboardComponent implements OnInit {

  layouts = [
    {text: 'One', cols: 4, rows: 4, color: 'lightblue'},
    {text: 'Two', cols: 12, rows: 12, color: 'lightgreen'},
    {text: 'Three', cols: 4, rows: 8, color: 'lightpink'},
  ];

  tokenLists:any;

  records = [
    {
      no: 'xxxxxxxxx01',
      date: '2017-11-09 10:23:45',
      status: 'SUBMITED',
      currency: 'USD',
      token: 'USD',
      amount: 1000,
      code: 'X50Y930D',
      bank: 'BOCHK xxxxxxxxxx'
    },{
      no: 'xxxxxxxxx02',
      date: '2017-11-10 15:53:45',
      status: 'MINT',
      currency: 'USD',
      token: 'USD',
      amount: 2000,
      code: 'X5A8Y930D',
      bank: 'BOCHK xxxxxxxxxx'
    }

  ];

  selectIndex: string;

  radioValue: string;


  currentAccount: string;

  public depositRequests: DepositRequest[] = [];
  //table
  displayedColumns = ['date', 'id',  'userId', 'currency','amount','state'];
  dataSource = new MatTableDataSource();


  constructor(
    private oauthService: OAuthService,
    private location: Location, private router: Router,
    private authService: AuthService,
    public web3Service: Web3Service,
    private _depositService: DepositService
  ) {

    //monitor if there are accounts available
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.currentAccount = accounts[0];
    });

    this.tokenLists = this.web3Service.tokenLists;
  }

  ngOnInit() {
    this.router.navigate([this.location.path()]);
    this._depositService.allRequest().subscribe(x => {
      this.depositRequests = x;
      this.dataSource.data = this.depositRequests;
    })
  }

}
