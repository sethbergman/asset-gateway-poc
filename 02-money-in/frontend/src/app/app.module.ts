import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AppRoutingModule} from './app-routing.module';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatGridListModule,
  MatInputModule,
  MatRadioModule,
  MatMenuModule,
  MatSelectModule,
  MatTableModule,
  MatToolbarModule,
  MatIconModule,
  MatSnackBarModule,
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HomeComponent} from './home/home.component';
import { GatewayDetailComponent } from './gateway/gateway-detail/gateway-detail.component';
import { GatewayDashboardComponent } from './gateway/gateway-dashboard/gateway-dashboard.component';
import { GatewaySubmitComponent } from './gateway/gateway-submit/gateway-submit.component';
import { GatewayTransferComponent } from './gateway/gateway-transfer/gateway-transfer.component';
import { GatewayMintComponent } from './gateway/gateway-mint/gateway-mint.component';
import {AuthService} from './services/auth.service';
import {AuthGuard} from './services/auth-guard.service';
import {LoginComponent} from './user/login/login.component';
import {RegisterComponent} from './user/register/register.component';
import { OverviewComponent } from './admin/overview/overview.component';
import { VerifyRequestComponent } from './admin/verify-request/verify-request.component';
import { MintTokenComponent } from './admin/mint-token/mint-token.component';
import {UtilModule} from "./util/util.module";
import {RegisterDoneComponent} from './user/register/register-done.component';
import {GatewayLeftnavComponent} from './gateway/gateway-leftnav/gateway-leftnav.component';
import {AdminLeftnavComponent} from './admin/admin-leftnav/admin-leftnav.component';
import {GatewayWithdrawMintComponent} from "./gateway/gateway-withdraw-mint/gateway-withdraw-mint.component";
import {BurnRequestComponent} from "./admin/burn-request/burn-request.component";
import {WithdrawComponent} from "./admin/withdraw/withdraw.component";
import {TansferRequestComponent} from "./admin/tansfer-request/tansfer-request.component";
import {GatewayWithdrawSubmitComponent} from "./gateway/gateway-withdraw-submit/gateway-withdraw-submit.component";
import {GatewayWithdrawTransferComponent} from "./gateway/gateway-withdraw-transfer/gateway-withdraw-transfer.component";
import {KycprocessRequestComponent} from "./admin/kycprocess-request/kycprocess-request.component";
import {KycprocessComponent} from "./admin/kycprocess/kycprocess.component";
import {GatewayKycprocessVerifyComponent} from "./gateway/gateway-kycprocess-verify/gateway-kycprocess-verify.component";
import {GatewayKycprocessComponent} from "./gateway/gateway-kycprocess/gateway-kycprocess.component";
import {ImageUploadModule} from 'angular2-image-upload';
import {OAuthModule} from 'angular-oauth2-oidc';
import {DepositService} from "./services/deposit.service";
import {WithdrawService} from "./services/withdraw.service";


// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, 'i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GatewayDetailComponent,
    GatewayDashboardComponent,
    RegisterComponent,
    LoginComponent,
    GatewaySubmitComponent,
    GatewayTransferComponent,
    GatewayMintComponent,
    OverviewComponent,
    VerifyRequestComponent,
    MintTokenComponent,
    RegisterDoneComponent,
    GatewayLeftnavComponent,
    AdminLeftnavComponent,
    AdminLeftnavComponent,
    WithdrawComponent,
    BurnRequestComponent,
    TansferRequestComponent,
    GatewayWithdrawSubmitComponent,
    GatewayWithdrawTransferComponent,
    GatewayWithdrawMintComponent,
    GatewayKycprocessComponent,
    GatewayKycprocessVerifyComponent,
    KycprocessComponent,
    KycprocessRequestComponent



  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    NgxDatatableModule,

    MatSelectModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatDialogModule,
    MatCardModule,
    MatGridListModule,
    MatToolbarModule,
    MatIconModule,
    MatRadioModule,
    MatSnackBarModule,
    UtilModule,
    ImageUploadModule.forRoot(),
    OAuthModule.forRoot()



  ],
  providers: [AuthService, AuthGuard, DepositService, WithdrawService],
  bootstrap: [AppComponent]
})
export class AppModule { }
