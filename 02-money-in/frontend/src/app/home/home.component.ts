import { Component, OnInit } from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private _oauthService: OAuthService,) {
  }

  ngOnInit() {

  }

  signin() {

    this._oauthService.initImplicitFlow()
  }

}
