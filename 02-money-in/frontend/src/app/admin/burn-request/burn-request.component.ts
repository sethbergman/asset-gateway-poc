import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {Web3Service} from "../../util/web3.service";
import {DepositRequest, DepositService} from "../../services/deposit.service";
import {Router, ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {WithdrawRequest, WithdrawService} from "../../services/withdraw.service";
import {async} from "q";

@Component({
  selector: 'app-burn-request',
  templateUrl: './burn-request.component.html',
  styleUrls: ['./burn-request.component.scss']
})
export class BurnRequestComponent implements OnInit {

  verifyStatus: number = 0;
  toggle = false;

  actions= [{
    name: 'Mint',
  },{
    name: 'Redeeption',
  },{
    name: 'Amount',
  }];

  rows = [{
    no: '001',
    type: 'Mint',
    userId: '0891231883839',
    ccy: 'USD',
    amount: '127287383.45',
    date: '10 Oct 2017',
    status: 1,
    action: 1,
  }];
  public withdrawRequest: any = {};

  //the balance of the user Todo probably wrap it in a user object
  public userBalance:string;

  constructor(private location: Location,
              private _web3Service: Web3Service,
              private _router: Router,
              private _route: ActivatedRoute,
              public snackBar: MatSnackBar,
              private withdrawService: WithdrawService) {
  }

  ngOnInit() {
    const id: Observable<string> = this._route.params.map(p => p.id);
    this.withdrawService.requestById(id)
      .subscribe(async x => {

        this.userBalance = await this._web3Service.balanceOf(x.address);
        this.withdrawRequest = x
      })
  }

  async burn() {
    await this._web3Service.burn(this.withdrawRequest.address, this.withdrawRequest.amount)
    this._router.navigate(['/en-US/admin/withdraw']);
  }

  goBack(): void {
    this.location.back();
  }

  chage(): void{
    this.toggle = true;
  }
}
