import { Component, OnInit } from '@angular/core';
import {Web3Service} from "../../util/web3.service";
import {DepositRequest, DepositService} from "../../services/deposit.service";
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {Location} from '@angular/common';
import {async} from "q";

@Component({
  selector: 'app-mint-token',
  templateUrl: './mint-token.component.html',
  styleUrls: ['./mint-token.component.scss']
})
export class MintTokenComponent implements OnInit {
  verifyStatus: number = 0;
  mintingFlag = false;

  actions= [{
    name: 'Mint',
  },{
    name: 'Redemption',
  },{
    name: 'Amount',
  }];


  public depositRequest: any = {};

  //the balance of the user Todo probably wrap it in a user object
  public userBalance:string;

  constructor(private location: Location,
              private _web3Service: Web3Service,
              private _router: Router,
              private _route: ActivatedRoute,
              public snackBar: MatSnackBar,
              private _depositService: DepositService) {
  }

  ngOnInit() {
    const id: Observable<string> = this._route.params.map(p => p.id);
    this._depositService.requestById(id)
      .subscribe(async x => {


        this.userBalance = await this._web3Service.balanceOf(x.address);
        this.depositRequest = x
      })
  }

  async mint(){
    await this._web3Service.mint(this.depositRequest.address, this.depositRequest.amount)
    this._router.navigate(['/en-US/admin/overview']);
  }

  goBack(): void {
    this.location.back();
  }


}
