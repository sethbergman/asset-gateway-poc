import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {Web3Service} from "../../util/web3.service";
import {Router, ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {DepositRequest, DepositService} from "../../services/deposit.service";
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-verify-request',
  templateUrl: './verify-request.component.html',
  styleUrls: ['./verify-request.component.scss']
})
export class VerifyRequestComponent implements OnInit {

  verifyStatus: number = 0;
  toggle = false;

  actions= [{
    name: 'Mint',
  },{
    name: 'Redeeption',
  },{
    name: 'Amount',
  }];

  rows = [{
    no: '001',
    type: 'Mint',
    userId: '0891231883839',
    ccy: 'USD',
    amount: '127287383.45',
    date: '10 Oct 2017',
    status: 1,
    action: 1,
  }];


  public depositRequest: DepositRequest;

  constructor(private location: Location,
              private _web3Service: Web3Service,
              private _router: Router,
              private _route: ActivatedRoute,
              public snackBar: MatSnackBar,
              private _depositService: DepositService) {
  }

  ngOnInit() {
    const id: Observable<string> = this._route.params.map(p => p.id);
    this._depositService.requestById(id)
      .subscribe(x => this.depositRequest = x)
  }

  async verify() {
    await this._web3Service.mint(this.depositRequest.address, this.depositRequest.amount)
  }

  goBack(): void {
    this.location.back();
  }

  chage(): void{
    this.toggle = true;
  }
}
