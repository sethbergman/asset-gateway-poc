import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-kycprocess-request',
  templateUrl: './kycprocess-request.component.html',
  styleUrls: ['./kycprocess-request.component.scss']
})
export class KycprocessRequestComponent implements OnInit {

  verifyStatus = 0;
  toggle1 = false;
  toggle2 = false;
  toggle3 = false;


  actions= [{
    name: 'Mint',
  }, {
    name: 'Redeeption',
  }, {
    name: 'Amount',
  }];

  rows = [{
    no: '001',
    type: 'Mint',
    userId: '0891231883839',
    ccy: 'USD',
    amount: '127287383.45',
    date: '10 Oct 2017',
    status: 1,
    action: 1,
  }];

  constructor(private location: Location) { }

  ngOnInit() {
  }

  verify() {
    setTimeout(() => {
      this.verifyStatus = 1;
    }, 2000);
  }

  goBack(): void {
    this.location.back();
  }

  chage1(): void {
    this.toggle1 = true;
    setTimeout(() => {
      this.toggle1 = false;
    }, 2000);
  }
  chage2(): void {
    this.toggle2 = true;
    setTimeout(() => {
      this.toggle2 = false;
    }, 2000);
  }
  chage3(): void {
    this.toggle3 = true;
  }
}
