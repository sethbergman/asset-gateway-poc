import { Component, OnInit } from '@angular/core';
import {DepositRequest, DepositService} from "../../services/deposit.service";
import {WithdrawRequest, WithdrawService} from "../../services/withdraw.service";
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit {

  actions = [{
    name: 'Tansfer',
  }, {
    name: 'Redeeption',
  }, {
    name: 'Amount',
  }];


  public withdrawRequests: WithdrawRequest[] = [];




  //table
  displayedColumns = ['date', 'id',  'userId', 'currency','amount','state','action'];
  dataSource = new MatTableDataSource();



  constructor(private withdrawService: WithdrawService) {
  }

  ngOnInit() {
    this.withdrawService.allRequest().subscribe(x => {
      this.withdrawRequests = x;
      this.dataSource.data = this.withdrawRequests;
    })

  }

}
