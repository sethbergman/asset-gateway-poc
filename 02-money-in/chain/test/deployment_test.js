const {expect, solc, ganacheWeb3} = require('chain-dsl/test/helpers')
const deployer = require('../lib/deployer')

describe('Deployment', () => {
    let web3, snaps, accounts, DEPLOYER, OPERATOR, CUSTOMER, gate

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({gate} = await deployer.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER, OPERATOR))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it('Gate is deployed', async () => {
        const symbol = web3.utils.hexToUtf8((await gate.methods.symbol().call()))
        expect(symbol).equal('TUSD')
    })
})
