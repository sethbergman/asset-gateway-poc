# 02. _Money In_ phase — Smart contracts

## Setup

```bash
yarn install
```

## Develop

Start a `ganache-core` chain, compile and deploy the current version of the contracts and listen on port 8545.

```bash
yarn start
```

The state of the chain will be persisted into the `./data` directory, to make sure that the nonce won't be reset and trigger a MetaMask issue. 

The result of the contract deployment is saved under `./out/{Gate,AccessControl,...}.json`, which contains and a contract `address` property and a `jsonInterface` property, which can be used from client apps to talk to the contract instance.

It's also possible to continue with the latest state of the chain  without redeployment:

```bash
yarn start --no-deploy
```

There is a debug web app which lives under the `./src` directory.

It can be started with

```bash
yarn dev
```

It's using the `lite-server` package which injects a `browser-sync` plugin into the served HTML pages, so they reload automatically when source files change.

The webpage itself is a vanilla HTML page, which only loads `web3.js@1.0.0-beta27` and wraps the `web3.currentProvider` injected by MetaMask.
 
The `web3.js` package was obtained via
 
 ```bash
curl -LO https://github.com/ethereum/web3.js/raw/1.0/dist/web3.js
```

## Test

Run the test suite continuously to follow both test source code and
smart contract code changes:

```bash
yarn test --watch
```
