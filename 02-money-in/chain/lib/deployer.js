const {create} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER, OPERATOR) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        Gate,
        AccessControl
    } = contractRegistry

    const accessControl = await deploy(AccessControl)
    const gate = await deploy(Gate, accessControl.options.address)

    const OPERATOR_ROLE = await accessControl.methods.OPERATOR().call()

    const allow = method => {
        const methodSig = web3.eth.abi.encodeFunctionSignature(method)
        return accessControl.methods
            .setRoleCapability(OPERATOR_ROLE, gate.options.address, methodSig, true)
            .send()
    }

    const methodsAllowedForOperator = [
        'balanceOf(address)',
        'mint(uint256)',
        'mint(address,uint256)',
        'mintFor(address,uint256)',
        'burn(address,uint256)',
        'burn(uint256)',
        'burnFrom(address,uint256)',
    ]

    await Promise.all([
        accessControl.methods
            .setUserRole(OPERATOR, OPERATOR_ROLE, true)
            .send(),
        ...methodsAllowedForOperator.map(allow)
    ])

    return {gate, accessControl}
}

module.exports = {
    base
}
