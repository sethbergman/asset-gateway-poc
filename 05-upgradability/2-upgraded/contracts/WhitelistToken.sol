pragma solidity 0.4.19;


import './dappsys.sol';


contract WhitelistToken is DSTokenBase {
    mapping (address => bool) public whitelisted;

    modifier isAllowed(address dst) {
        assert(whitelisted[dst]);
        _;
    }

    function WhitelistToken() DSTokenBase(1000) public {}

    function transfer(address dst, uint wad) public isAllowed(dst) returns (bool) {
        return super.transfer(dst, wad);
    }

    function whitelist(address guy) public {
        whitelisted[guy] = true;
    }
}
