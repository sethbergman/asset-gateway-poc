const {create} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        Token
    } = contractRegistry

    // if (token.isDeployed()) {
    //     const token = new web3.Contract(Token).at('0x1234...')
    // } else {
    //     const token = await deploy(Token)
    //     // save token.address & token.options.jsonInterface
    // }

    const token = await deploy(Token)

    return {token}
}

const whitelist = async (web3, contractRegistry, prev, DEPLOYER) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        WhitelistToken
    } = contractRegistry

    const whitelistToken = await deploy(WhitelistToken)
    return Object.assign({}, prev, {whitelistToken});
}

const latest = async (web3, contractRegistry, DEPLOYER) => {
    const initialDeployment = await base(web3, contractRegistry, DEPLOYER)
    return await whitelist(web3, contractRegistry, initialDeployment, DEPLOYER)
}

module.exports = {
    base,
    whitelist,
    latest
}
